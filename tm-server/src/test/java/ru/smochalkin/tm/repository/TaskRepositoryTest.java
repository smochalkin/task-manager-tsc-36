package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void init() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();

        for (int i = 0; i < ENTRY_COUNT; i++) {
            @NotNull final Task task = new Task();
            task.setName("name " + i);
            task.setDescription("description " + i);
            if ((i < ENTRY_COUNT / 2)) {
                task.setUserId(USER_ID_1);
                task.setProjectId(PROJECT_ID_1);
            }else {
                task.setUserId(USER_ID_2);
                task.setProjectId(PROJECT_ID_2);
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void addTest() {
        @NotNull final Task newTask = new Task();
        taskRepository.add(newTask);
        Assert.assertEquals(ENTRY_COUNT + 1, taskRepository.getCount());
    }

    @Test
    public void addAllTest() {
        @NotNull final int expectedSize = taskList.size() + taskRepository.getCount();
        taskRepository.addAll(taskList);
        Assert.assertEquals(expectedSize, taskRepository.getCount());
    }

    @Test
    public void clearTest() {
        taskRepository.clear();
        Assert.assertEquals(0, taskRepository.getCount());
    }


    @Test
    public void clearByUserTest() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        taskRepository.clear(USER_ID_1);
        Assert.assertEquals(0, taskRepository.findAll(USER_ID_1).size());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll();
        Assert.assertEquals(taskList, actualTaskList);
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Task> tasks = taskRepository.findAll(USER_ID_1);
        Assert.assertEquals(ENTRY_COUNT / 2, tasks.size());
        Assert.assertEquals(USER_ID_1, tasks.get(0).getUserId());
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findById(UUID.randomUUID().toString()));
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.findById(task.getId()));
            Assert.assertEquals(task, taskRepository.findById(task.getId()));
        }
    }


    @Test
    public void findByIdAndByUserIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findById(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Task task : taskList) {
            if (USER_ID_1.equals(task.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            Assert.assertEquals(task, taskRepository.findByName(userId, task.getName()));
        }
    }

    @Test
    public void findByNameTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findByName(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Task task : taskList) {
            if (USER_ID_1.equals(task.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            Assert.assertEquals(task, taskRepository.findByName(userId, task.getName()));
        }
    }

    @Test
    public void findByIndexTest() {
        @NotNull final Task task1 = taskList.get(0);
        @NotNull final Task task2 = taskList.get(ENTRY_COUNT / 2);
        Assert.assertEquals(task1, taskRepository.findByIndex(USER_ID_1, 0));
        Assert.assertEquals(task2, taskRepository.findByIndex(USER_ID_2, 0));
    }

    @Test
    public void existsByIdTest() {
        @NotNull final String isId = taskList.get(0).getId();
        @NotNull final String notId = UUID.randomUUID().toString();
        Assert.assertTrue(taskRepository.existsById(isId));
        Assert.assertFalse(taskRepository.existsById(notId));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(ENTRY_COUNT, taskRepository.getCount());
    }

    @Test
    public void removeTest() {
        for (@NotNull final Task task : taskList) {
            taskRepository.remove(task);
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.removeById(task.getId()));
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void removeByIdAndByUserIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.removeById(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Task task : taskList) {
            if (USER_ID_1.equals(task.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            taskRepository.removeById(userId, task.getId());
            @NotNull final String uId = userId;
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> taskRepository.findById(uId, task.getId()));
        }
    }

    @Test
    public void removeByNameTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.removeByName(USER_ID_1, "not found"));
        @NotNull String userId;
        for (@NotNull final Task task : taskList) {
            if (USER_ID_1.equals(task.getUserId()))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            taskRepository.removeByName(userId, task.getName());
            @NotNull final String uId = userId;
            Assert.assertThrows(EntityNotFoundException.class,
                    () -> taskRepository.findByName(uId, task.getName()));
        }
    }

    @Test
    public void removeByIndexTest() {
        @NotNull final Task task1 = taskList.get(0);
        @NotNull final Task task2 = taskList.get(ENTRY_COUNT / 2);
        taskRepository.removeByIndex(USER_ID_1, 0);
        Assert.assertNotEquals(task1, taskRepository.findByIndex(USER_ID_1, 0));
        taskRepository.removeByIndex(USER_ID_2, 0);
        Assert.assertNotEquals(task2, taskRepository.findByIndex(USER_ID_2, 0));
    }

    @Test
    public void removeAllTest() {
        taskRepository.removeAll(taskList);
        Assert.assertEquals(0, taskRepository.getCount());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final Task task = taskList.get(0);
        @NotNull final String taskId = task.getId();
        @NotNull final String testName = "test name";
        @NotNull final String testDesc = "test desc";
        task.setName(testName);
        task.setDescription(testDesc);
        Assert.assertEquals(testName, task.getName());
        Assert.assertEquals(testDesc, task.getDescription());
        Assert.assertEquals(task, taskRepository.findById(taskId));
    }

    @Test
    public void updateByIdAndByUserIdTest() {
        @NotNull final Task task = taskList.get(0);
        @NotNull final String taskId = task.getId();
        @NotNull final String testName = "test name";
        @NotNull final String testDesc = "test desc";
        task.setName(testName);
        task.setDescription(testDesc);
        Assert.assertEquals(testName, task.getName());
        Assert.assertEquals(testDesc, task.getDescription());
        Assert.assertEquals(task, taskRepository.findById(USER_ID_1, taskId));
    }

    @Test
    public void updateByIndexTest() {
        @NotNull final Task task = taskList.get(0);
        @NotNull final String testName = "test name";
        @NotNull final String testDesc = "test desc";
        task.setName(testName);
        task.setDescription(testDesc);
        Assert.assertEquals(testName, task.getName());
        Assert.assertEquals(testDesc, task.getDescription());
        Assert.assertEquals(task, taskRepository.findByIndex(USER_ID_1, 0));
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final Task task = taskList.get(0);
        @NotNull final String taskId = task.getId();
        @Nullable final Date startDate = task.getStartDate();
        taskRepository.updateStatusById(USER_ID_1, taskId, Status.IN_PROGRESS);
        @NotNull final Task actualTask = taskRepository.findById(taskId);
        Assert.assertEquals(task, actualTask);
        Assert.assertNotEquals(startDate, actualTask.getStartDate());
    }

    @Test
    public void updateStatusByNameTest() {
        @NotNull final Task task = taskList.get(0);
        @NotNull final String taskName = task.getName();
        @Nullable final Date startDate = task.getStartDate();
        taskRepository.updateStatusByName(USER_ID_1, taskName, Status.IN_PROGRESS);
        @NotNull final Task actualTask = taskRepository.findByName(USER_ID_1, taskName);
        Assert.assertEquals(task, actualTask);
        Assert.assertNotEquals(startDate, actualTask.getStartDate());
    }

    @Test
    public void updateStatusByIndexTest() {
        @NotNull final Task task = taskList.get(0);
        @Nullable final Date endDate = task.getEndDate();
        taskRepository.updateStatusByIndex(USER_ID_1, 0, Status.COMPLETED);
        @NotNull final Task actualTask = taskRepository.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(task, actualTask);
        Assert.assertNotEquals(endDate, actualTask.getEndDate());
    }

    @Test
    public void getCountByUserTest() {
        Assert.assertEquals(ENTRY_COUNT / 2, taskRepository.getCountByUser(USER_ID_1));
    }

    @Test
    public void bindTaskByIdTest() {
        @NotNull final Task task = taskList.get(0);
        Assert.assertEquals(PROJECT_ID_1, task.getProjectId());
        @NotNull final String taskId = task.getId();
        taskRepository.bindTaskById(PROJECT_ID_2, taskId);
        Assert.assertEquals(PROJECT_ID_2, task.getProjectId());
    }

    @Test
    public void bindTaskByIdAndByUserIdTest() {
        @NotNull final Task task = taskList.get(0);
        Assert.assertEquals(PROJECT_ID_1, task.getProjectId());
        @NotNull final String taskId = task.getId();
        taskRepository.bindTaskById(USER_ID_1, PROJECT_ID_2, taskId);
        Assert.assertEquals(PROJECT_ID_2, task.getProjectId());
    }

    @Test
    public void unbindTaskByIdTest() {
        @NotNull final Task task = taskList.get(0);
        Assert.assertEquals(PROJECT_ID_1, task.getProjectId());
        @NotNull final String taskId = task.getId();
        taskRepository.unbindTaskById(taskId);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void unbindTaskByIdAndByUserIdTest() {
        @NotNull final Task task = taskList.get(0);
        Assert.assertEquals(PROJECT_ID_1, task.getProjectId());
        @NotNull final String taskId = task.getId();
        taskRepository.unbindTaskById(USER_ID_1, taskId);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void findTasksByProjectIdTest() {
        List<Task> taskProject1List = taskList.subList(0, ENTRY_COUNT / 2);
        Assert.assertEquals(taskProject1List, taskRepository.findTasksByProjectId(PROJECT_ID_1));
    }

    @Test
    public void findTasksByProjectIdUserIdTest() {
        List<Task> taskProject1List = taskList.subList(0, ENTRY_COUNT / 2);
        Assert.assertEquals(taskProject1List, taskRepository.findTasksByProjectId(USER_ID_1, PROJECT_ID_1));
    }

    @Test
    public void removeTasksByProjectIdTest() {
        taskRepository.removeTasksByProjectId(PROJECT_ID_1);
        Assert.assertEquals(0, taskRepository.findTasksByProjectId(PROJECT_ID_1).size());
    }

}
