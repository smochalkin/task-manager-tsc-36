package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.service.ServiceLocator;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(final @NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
