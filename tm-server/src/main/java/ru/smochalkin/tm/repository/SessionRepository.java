package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.model.Session;

import java.util.List;
import java.util.stream.Collectors;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    public List<Session> findAllByUserId(@Nullable String userId) {
        return list.stream().filter(e -> e.getUserId().equals(userId)).collect(Collectors.toList());
    }

}
