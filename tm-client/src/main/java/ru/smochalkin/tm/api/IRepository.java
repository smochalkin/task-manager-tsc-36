package ru.smochalkin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@Nullable E entity);

    void addAll(@NotNull List<E> entities);

    void clear();

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@Nullable String id);

    @NotNull
    E remove(@NotNull E entity);

    @NotNull
    E removeById(@Nullable String id);

    int getCount();

}